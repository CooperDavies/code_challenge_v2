﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public enum Heading
{
    ZPos, ZNeg, XPos, XNeg
}

public abstract class Ant : MonoBehaviour
{
    public int id = -1;
    public int X, Y, Z;
    public Heading Facing = Heading.ZPos;
    public int health;
    public int NumberOfBlocksHeld = 0;

    public void Init(int i, int x, int y, int z, int h)
    {
        id = i; X = x; Y = y; Z = z; health = h;
    }

    protected bool Move()
    {
        int newZ = Z;
        int newX = X;
        if (Facing == Heading.XNeg)
            newX--;
        if (Facing == Heading.XPos)
            newX++;
        if (Facing == Heading.ZNeg)
            newZ--;
        if (Facing == Heading.ZPos)
            newZ++;
        int yDiff = GetYDiff(newX, Y, newZ);

        if (yDiff >= 1)
        {
            return false;
        }
        if (newZ < 0 || newX < 0 || newZ > Vars.WorldWidth - 2 || newX > Vars.WorldWidth - 2 || Y + yDiff > Vars.WorldHeight - 1)
        {
            return false;
        }
        X = newX;
        Y += yDiff + 1;
        Z = newZ;

        transform.position = new Vector3(X, Y, Z);
        return true;
    }

    /// <summary>
    /// Gets the difference in height between the current y value, and the new X, Z Values
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <param name="z"></param>
    /// <returns></returns>
    protected int GetYDiff(int x, int y, int z)
    {
        for (int i = Vars.WorldHeight; i > 1; i--)
        {
            
            if (WorldManager.Instance[x, i, z] != (byte)BlockTypes.Air)
            {
                return i - y;
            }
        }
        return y;
    }

    protected void Turn(Heading heading)
    {
        Facing = heading;
        if (Facing == Heading.XPos)
            transform.LookAt(transform.position + Vector3.right);
        if (Facing == Heading.XNeg)
            transform.LookAt(transform.position + Vector3.left);
        if (Facing == Heading.ZPos)
            transform.LookAt(transform.position + Vector3.forward);
        if (Facing == Heading.ZNeg)
            transform.LookAt(transform.position + Vector3.back);

    }

    protected bool DigForward()
    {
        if (NumberOfBlocksHeld > Vars.MaxBlocksHeld)
        {
            return false;
        }

        if (Y >= Vars.WorldHeight - 1)
            return false;

        int newZ = Z;
        int newX = X;

        if (Facing == Heading.XNeg)
            newX--;
        if (Facing == Heading.XPos)
            newX++;
        if (Facing == Heading.ZNeg)
            newZ--;
        if (Facing == Heading.ZPos)
            newZ++;
        if (WorldManager.Instance.NumAntsOn(newX, Y + 1, newZ) < 1)
        {
            if (TerrainEditor.Instance.SetBlockAt(newX, Y, newZ, (byte)BlockTypes.Air))
            {
                NumberOfBlocksHeld++;
                return true;
            }
        }
        return false;
    }

    protected bool DigDown()
    {
        if (NumberOfBlocksHeld > Vars.MaxBlocksHeld)
            return false;


        if (WorldManager.Instance.NumAntsOn(X, Y, Z) < 2)
        {
            if (TerrainEditor.Instance.SetBlockAt(X, Y - 1, Z, (byte)BlockTypes.Air))
            {
                NumberOfBlocksHeld++;
                Y--;
                transform.position = transform.position + Vector3.down;
                return true;
            }
        }
        return false;
    }

    protected bool PlaceForward()
    {
        if (NumberOfBlocksHeld <= 0)
            return false; ;
        if (Y >= Vars.WorldHeight - 2)
            return false;
        int newZ = Z;
        int newX = X;

        if (Facing == Heading.XNeg)
            newX--;
        if (Facing == Heading.XPos)
            newX++;
        if (Facing == Heading.ZNeg)
            newZ--;
        if (Facing == Heading.ZPos)
            newZ++;

        if (Y >= Vars.WorldHeight - 1)
            return false;

        if (WorldManager.Instance[newX, Y, newZ] != (byte)BlockTypes.Air)
            return false;

        if (WorldManager.Instance.NumAntsOn(newX, Y, newZ) < 1)
        {
            TerrainEditor.Instance.SetBlockAt(newX, Y, newZ, (byte)BlockTypes.Stone);
            NumberOfBlocksHeld--;
            return true;
        }
        return false;
    }

    protected bool PlaceDown()
    {
        if (NumberOfBlocksHeld <= 0)
            return false; ;

        if (Y >= Vars.WorldHeight - 1)
            return false;
        if (WorldManager.Instance.NumAntsOn(X, Y, Z) < 2)
        {
            TerrainEditor.Instance.SetBlockAt(X, Y, Z, (byte)BlockTypes.Stone);
            NumberOfBlocksHeld--;
            Y++;
            transform.position = transform.position + Vector3.up;
            return true;
        }
        return false;
    }

    protected bool EatDown()
    {
        if (WorldManager.Instance[X, Y, Z] == (byte)BlockTypes.Food ||
            WorldManager.Instance[X, Y, Z] == (byte)BlockTypes.Grass)
        {
            if (WorldManager.Instance.NumAntsOn(X, Y, Z) < 2)
            {
                TerrainEditor.Instance.SetBlockAt(X, Y - 1, Z, (byte)BlockTypes.Air);
                if (this is Worker)
                    health = Vars.MaxWorkerHealth;
                else if (this is Scavenger)
                    health = Vars.MaxScavengerHealth;
                else
                    health = Vars.MaxQueenHealth;
                Y--;
                transform.position = transform.position + Vector3.down;
                return true;
            }
        }
        return false;
    }

    protected bool EatForward()
    {
        int newZ = Z;
        int newX = X;

        if (Y >= Vars.WorldHeight - 1)
            return false;

        if (Facing == Heading.XNeg)
            newX--;
        if (Facing == Heading.XPos)
            newX++;
        if (Facing == Heading.ZNeg)
            newZ--;
        if (Facing == Heading.ZPos)
            newZ++;

        if (WorldManager.Instance[newX, Y, newZ] == (byte)BlockTypes.Food ||
            WorldManager.Instance[newX, Y, newZ] == (byte)BlockTypes.Grass)
        {
            if (WorldManager.Instance.NumAntsOn(newX, Y, newZ) < 1)
            {
                TerrainEditor.Instance.SetBlockAt(newX, Y, newZ, (byte)BlockTypes.Air);
                if (this is Worker)
                    health = Vars.MaxWorkerHealth;
                else if (this is Scavenger)
                    health = Vars.MaxScavengerHealth;
                else
                    health = Vars.MaxQueenHealth;
                return true;
            }
        }
        return false;
    }

}



