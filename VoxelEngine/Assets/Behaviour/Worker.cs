﻿using System;
using UnityEngine;

public class Worker : Ant
{

    public void FixedUpdate()
    {
        /*
         * THIS IS WHERE YOU WILL NEED TO IMPLEMENT BEHAVIOUR.
         * HERE YOU SEE THAT MY ANTS BASICALLY MAKE RANDOM CHOICES
         */
         
        health-=Vars.TurnCost;
        if (health <= 0)
            Destroy(this.gameObject);


        if (health < Vars.MaxWorkerHealth / 3) {
            if (!EatDown())
                if (!EatForward())
                    DoRandom();
        }
        else
        {
            DoRandom();
        }
    }

    public void DoRandom()
    {
        float prob = (float)WorldManager.Instance.rng.NextDouble();
        if (prob > .5)
        {
            Move();
        }
        else
        {
            if (prob < 0.15)
            {

                Turn((Heading)WorldManager.Instance.rng.Next(0, 4));
            }
            else
            {
                int fwd = WorldManager.Instance.rng.Next(0, 2);
                if (NumberOfBlocksHeld == 0)
                    if (fwd == 0)
                    {
                        DigDown();
                    }
                    else
                    {
                        DigForward();
                    }

                else if (NumberOfBlocksHeld >= Vars.MaxBlocksHeld)
                    if (fwd == 0)
                    {
                        PlaceDown();
                    }
                    else
                    {
                        PlaceForward();
                    }
                else
                {
                    int digPlace = WorldManager.Instance.rng.Next(0, 2);
                    if (digPlace == 0)
                        if (fwd == 0)
                        {
                            DigForward();
                        }
                        else
                        {
                            DigDown();
                        }
                    else
                    {
                        if (fwd == 0)
                        {
                            PlaceForward();
                        }
                        else
                        {
                            PlaceDown();
                        }
                    }
                }

            }
        }
    }

}



