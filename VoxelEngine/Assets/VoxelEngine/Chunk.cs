﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class Chunk : MonoBehaviour
{
    #region Logic Vars

    public int X;
    public int Y;
    public int Z;
    

    #endregion

    #region Rendering Vars

    public Material mat;
    MeshCollider col;
    Mesh mesh;
    MeshRenderer render;
    List<Vector3> vertices = new List<Vector3>();
    List<int> triangles = new List<int>();
    List<Vector2> uvs = new List<Vector2>();
    int NumFaces = 0;

    public bool UpdateNeeded;

    #endregion

    public void Init()
    {
        mesh = gameObject.AddComponent<MeshFilter>().mesh;
        render = gameObject.AddComponent<MeshRenderer>();
        col = gameObject.AddComponent<MeshCollider>();
        render.material = mat;
        GenerateMesh();
    }

    public void GenerateMesh()
    {
        for (int x = 0; x < Vars.ChunkSize; x++)
            for (int z = 0; z < Vars.ChunkSize; z++)
                for (int y = 0; y < Vars.ChunkSize; y++) {
                    byte ours = Block(x, y, z);
                    if (ours!= (byte)BlockTypes.Air)
                    {
                        if (Block(x, y + 1, z) == (byte)BlockTypes.Air)
                            FaceYPos(x, y, z, ours);

                        if (Block(x, y - 1, z) == (byte)BlockTypes.Air)
                            FaceYNeg(x, y, z, ours);

                        if (Block(x + 1, y, z) == (byte)BlockTypes.Air)
                            FaceXPos(x, y, z, ours);

                        if (Block(x - 1, y, z) == (byte)BlockTypes.Air)
                            FaceXNeg(x, y, z, ours);

                        if (Block(x, y, z + 1) == (byte)BlockTypes.Air)
                            FaceZPos(x, y, z, ours);

                        if (Block(x, y, z - 1) == (byte)BlockTypes.Air)
                            FaceZNeg(x, y, z, ours);
                    }
                }
        UpdateMesh();
    }

    public byte Block(int x, int y, int z)
    {
        return WorldManager.Instance[x + X, y + Y, z + Z];
    }

    void UpdateMesh()
    {
        mesh.Clear();
        mesh.vertices = vertices.ToArray();
        mesh.triangles = triangles.ToArray();
        mesh.uv = uvs.ToArray();
        MeshUtility.Optimize(mesh);
        mesh.RecalculateNormals();
        col.sharedMesh = mesh;
        vertices.Clear();
        uvs.Clear();
        triangles.Clear();
        NumFaces = 0;
    }

    #region Mesh Creation

    void AddRecent6Triangles()
    {
        triangles.Add(NumFaces * 4);
        triangles.Add(NumFaces * 4 + 1);
        triangles.Add(NumFaces * 4 + 2);
        triangles.Add(NumFaces * 4);
        triangles.Add(NumFaces * 4 + 2);
        triangles.Add(NumFaces * 4 + 3);
        NumFaces++;
    }

    void AddRecentFaceTexture(byte blockType)
    {
        Vector2 tilSheet = Vars.BlockDictionary[blockType];
        uvs.Add(new Vector2(Vars.textureUnitRatio * tilSheet.x + Vars.textureUnitRatio, Vars.textureUnitRatio * tilSheet.y));
        uvs.Add(new Vector2(Vars.textureUnitRatio * tilSheet.x + Vars.textureUnitRatio, Vars.textureUnitRatio * tilSheet.y + Vars.textureUnitRatio));
        uvs.Add(new Vector2(Vars.textureUnitRatio * tilSheet.x, Vars.textureUnitRatio * tilSheet.y + Vars.textureUnitRatio));
        uvs.Add(new Vector2(Vars.textureUnitRatio * tilSheet.x, Vars.textureUnitRatio * tilSheet.y));
    }

    void FaceYPos(int x, int y, int z, byte blockType)
    {
        vertices.Add(new Vector3(x, y, z + 1));
        vertices.Add(new Vector3(x + 1, y, z + 1));
        vertices.Add(new Vector3(x + 1, y, z));
        vertices.Add(new Vector3(x, y, z));
        AddRecent6Triangles();
        AddRecentFaceTexture(blockType);
    }

    void FaceYNeg(int x, int y, int z, byte blockType)
    {
        vertices.Add(new Vector3(x, y - 1, z));
        vertices.Add(new Vector3(x + 1, y - 1, z));
        vertices.Add(new Vector3(x + 1, y - 1, z + 1));
        vertices.Add(new Vector3(x, y - 1, z + 1));
        AddRecent6Triangles();
        AddRecentFaceTexture(blockType);
    }

    void FaceZPos(int x, int y, int z, byte blockType)
    {
        vertices.Add(new Vector3(x + 1, y - 1, z + 1));
        vertices.Add(new Vector3(x + 1, y, z + 1));
        vertices.Add(new Vector3(x, y, z + 1));
        vertices.Add(new Vector3(x, y - 1, z + 1));
        AddRecent6Triangles();
        AddRecentFaceTexture(blockType);
    }

    void FaceXPos(int x, int y, int z, byte blockType)
    {
        vertices.Add(new Vector3(x + 1, y - 1, z));
        vertices.Add(new Vector3(x + 1, y, z));
        vertices.Add(new Vector3(x + 1, y, z + 1));
        vertices.Add(new Vector3(x + 1, y - 1, z + 1));
        AddRecent6Triangles();
        AddRecentFaceTexture(blockType);
    }

    void FaceZNeg(int x, int y, int z, byte blockType)
    {
        vertices.Add(new Vector3(x, y - 1, z));
        vertices.Add(new Vector3(x, y, z));
        vertices.Add(new Vector3(x + 1, y, z));
        vertices.Add(new Vector3(x + 1, y - 1, z));
        AddRecent6Triangles();
        AddRecentFaceTexture(blockType);
    }

    void FaceXNeg(int x, int y, int z, byte blockType)
    {
        vertices.Add(new Vector3(x, y - 1, z + 1));
        vertices.Add(new Vector3(x, y, z + 1));
        vertices.Add(new Vector3(x, y, z));
        vertices.Add(new Vector3(x, y - 1, z));
        AddRecent6Triangles();
        AddRecentFaceTexture(blockType);
    }

    private void LateUpdate()
    {
        if (UpdateNeeded)
        {
            GenerateMesh();
            UpdateNeeded = false;
        }
    }

    #endregion
}
