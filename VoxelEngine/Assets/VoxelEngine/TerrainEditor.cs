﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class TerrainEditor : MonoBehaviour
{
    public static TerrainEditor Instance;

    public void Start()
    {
        if (Instance == null)
            Instance = this;
        else
        {
            if (!ReferenceEquals(Instance, this))
                DestroyImmediate(this);
        }
    }

    //public void ReplaceBlockCenter(float range, byte block)
    //{
    //    //Replaces the block directly in front of the player
    //}

    //public void AddBlockCenter(float range, byte block)
    //{
    //    //Adds the block specified directly in front of the player
    //}

    public void ReplaceBlockCursor(byte block)
    {
        //Replaces the block specified where the mouse cursor is pointing

        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit))
        {
            ReplaceBlockAt(hit, block);
        }
    }

    public void AddBlockCursor(byte block)
    {
        //Adds the block specified where the mouse cursor is pointing

        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit))
        {

            AddBlockAt(hit, block);
        }
    }

    public void ReplaceBlockAt(RaycastHit hit, byte block)
    {
        Vector3 position = hit.point;
        position += (hit.normal * -0.5f);
        SetBlockAt(position, block);
    }

    public void AddBlockAt(RaycastHit hit, byte block)
    {
        //adds the specified block at these impact coordinates, you can raycast against the terrain and call this with the hit.point
        Vector3 position = hit.point;
        position += (hit.normal * 0.5f);

        SetBlockAt(position, block);

    }

    public void SetBlockAt(Vector3 position, byte block)
    {
        int x = Mathf.RoundToInt(position.x);
        int y = Mathf.RoundToInt(position.y);
        int z = Mathf.RoundToInt(position.z);

        SetBlockAt(x, y, z, block);
    }

    public bool SetBlockAt(int x, int y, int z, byte block)
    {
        if (WorldManager.Instance[x, y, z] == (byte)BlockTypes.Impervious)
        {
            return false;
        }
        WorldManager.Instance[x, y, z] = block;
        for (int i = -1; i < 2; i++)
            for (int j = -1; j < 2; j++)
                for (int k = -1; k < 2; k++)
                {
                    if (x + i < Vars.WorldWidth - 1 && y + j < Vars.WorldHeight - 1 && z + k < Vars.WorldWidth - 1)
                        WorldManager.Instance.UpdateChunkAt(x + i, y + j, z + k);
                }
        return true;

    }

    public void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            ReplaceBlockCursor((byte)BlockTypes.Air);
        }

        if (Input.GetMouseButtonDown(1))
        {
            AddBlockCursor((byte)BlockTypes.Stone);
        }
    }

}
