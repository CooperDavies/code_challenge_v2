﻿using System.Collections.Generic;
using UnityEngine;

public enum BlockTypes
{
    Air = 0,
    Stone = 14,
    Grass = 3,
    Food = 2,
    Hazard = 4,
    Impervious = 16,
}

public class Vars
{
    public static float textureUnitRatio = 0.25f;
    public static int ChunkSize = 16;
    public static int Seed = 348576;
    public static Dictionary<byte, Vector2> BlockDictionary = new Dictionary<byte, Vector2>()
    {
        {1,new Vector2(0,0)},
        {2,new Vector2(0,1)},
        {3,new Vector2(0,2)},
        {4,new Vector2(0,3)},
        {5,new Vector2(1,0)},
        {6,new Vector2(1,1)},
        {7,new Vector2(1,2)},
        {8,new Vector2(1,3)},
        {9,new Vector2(2,0)},
        {10,new Vector2(2,1)},
        {11,new Vector2(2,2)},
        {12,new Vector2(2,3)},
        {13,new Vector2(3,0)},
        {14,new Vector2(3,1)},
        {15,new Vector2(3,2)},
        {16,new Vector2(3,3)}

    };
    public static int WorldWidth = ChunkSize * 4;
    public static int WorldHeight = ChunkSize  * 3;
    public static int NumHazards = 18;
    public static int HazardRadius = (int)(0.5f * (WorldWidth / (float)ChunkSize));
    public static int NumImpervious = 4;
    public static int ImperviousRadius = (int)(1.5f * (WorldWidth / (float)ChunkSize));
    public static int NumAnts = 200;
    public static float WorkerToScavengerRatio = Mathf.Clamp(1f, 0f, 1f);
    public static int MaxWorkerHealth = 10000;
    public static int MaxScavengerHealth = 10000;
    public static int MaxQueenHealth = 1000;
    public static int MaxBlocksHeld = 3;
    public static int QueenPlacementCost = MaxQueenHealth / 3;
    public static int TurnCost = 1;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="x">x coord</param>
    /// <param name="y">y coord</param>
    /// <param name="z">z coord</param>
    /// <param name="scale">coord / scale = new coord</param>
    /// <param name="height">pResult * height = val</param>
    /// <param name="power">val ^ pow = return</param>
    /// <returns></returns>
    public static int DiscretePerlineNoise(int x, int y, int z, float scale, float height, float power)
    {
        float rValue;
        rValue = Noise.GetNoise(((double)x) / scale, ((double)y) / scale, ((double)z) / scale);
        rValue *= height;

        if (power != 0)
        {
            rValue = Mathf.Pow(rValue, power);
        }

        return (int)rValue;
    }
}

