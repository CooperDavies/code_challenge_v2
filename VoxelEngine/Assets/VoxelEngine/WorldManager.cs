﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class WorldManager : MonoBehaviour
{

    public static WorldManager Instance;

    public GameObject antPrefab;

    public GameObject ChunkPrefab;
    public System.Random rng;

    Chunk[,,] Chunks;
    byte[,,] data;
    List<Ant> ants = new List<Ant>();

    public byte this[int x, int y, int z]
    {
        get
        {
            if (x < 0 || x > Vars.WorldWidth - 1 || z < 0 || z > Vars.WorldWidth - 1 || y < 0 || y > Vars.WorldHeight - 1)
                return 0;
            return data[x, y, z];
        }
        set
        {
            data[x, y, z] = value;
        }
    }

    // Use this for initialization
    void Awake()
    {

        if (Instance == null)
            Instance = this;
        else
        {
            if (!ReferenceEquals(Instance, this))
                DestroyImmediate(this);
        }
        rng = new System.Random(Vars.Seed);
        GenerateData();
        GenerateChunks();
        PlaceAnts();
    }

    void PlaceAnts()
    {
        GameObject WorkerAnts = new GameObject("WorkerAnts");
        GameObject ScavengerAnts = new GameObject("ScavengerAnts");
        if (Vars.NumAnts >= Vars.WorldWidth * Vars.WorldWidth)
            throw new Exception("Cannot place more ants than spaces");
        for (int i = 0; i < Vars.NumAnts; i++)
        {
            int x, z;
            do
            {
                x = rng.Next(1, Vars.WorldWidth - 2);
                z = rng.Next(1, Vars.WorldWidth - 2);
            }
            while (!Placeable(x, z));
            for (int j = Vars.WorldHeight; j > 0; j--)
            {
                if (data[x, j - 1, z] != (byte)BlockTypes.Air)
                {
                    GameObject ant = Instantiate(antPrefab, new Vector3(x, j, z), new Quaternion(0, 0, 0, 0));
                    if (i == 0)
                    {
                        ant.name = "Queen";
                        ant.tag = "Queen";
                        ant.AddComponent<Queen>().Init(i, x, j, z, Vars.MaxQueenHealth);
                        ant.GetComponentInChildren<MeshRenderer>().material.color = Color.red;
                    }
                    else if (i < Vars.NumAnts * Vars.WorkerToScavengerRatio)
                    {
                        ant.transform.parent = WorkerAnts.transform;
                        ant.AddComponent<Worker>().Init(i, x, j, z, Vars.MaxWorkerHealth);
                        ant.GetComponentInChildren<MeshRenderer>().material.color = Color.blue;
                    }
                    else
                    {
                        ant.transform.parent = ScavengerAnts.transform;
                        ant.AddComponent<Scavenger>().Init(i, x, j, z, Vars.MaxScavengerHealth);
                        //Ask Julie why the fuck this number is this number
                        ant.GetComponentInChildren<MeshRenderer>().material.color = new Color(0.9528302f, 0.4486807f, 0.05842829f);
                    }
                    ants.Add(ant.GetComponent<Ant>());
                    break;
                }
            }
        }
    }

    bool Placeable(int x, int z)
    {
        for (int j = Vars.WorldHeight; j > 0; j--)
        {
            if (data[x, j - 1, z] != (byte)BlockTypes.Air)
            {
                if (data[x, j - 1, z] == (byte)BlockTypes.Hazard || NumAntsOn(x, j, z) > 0)
                    return false;
                else
                    return true;
            }
        }
        return false;
    }

    void GenerateChunks()
    {
        GameObject chunkObg = new GameObject("Chunks");
        Chunks = new Chunk[Mathf.FloorToInt(Vars.WorldWidth / Vars.ChunkSize),
            Mathf.FloorToInt(Vars.WorldHeight / Vars.ChunkSize),
            Mathf.FloorToInt(Vars.WorldWidth / Vars.ChunkSize)];

        for (int x = 0; x < Chunks.GetLength(0); x++)
            for (int z = 0; z < Chunks.GetLength(2); z++)
                for (int y = 0; y < Chunks.GetLength(1); y++)
                {
                    GameObject temp = Instantiate(ChunkPrefab,
                                                    new Vector3(x * Vars.ChunkSize - 0.5f, y * Vars.ChunkSize + 0.5f, z * Vars.ChunkSize - 0.5f),
                                                    new Quaternion(0, 0, 0, 0));
                    temp.transform.parent = chunkObg.transform;
                    Chunk chunkScript = temp.GetComponent<Chunk>();
                    chunkScript.X = x * Vars.ChunkSize; chunkScript.Y = y * Vars.ChunkSize; chunkScript.Z = z * Vars.ChunkSize;
                    chunkScript.Init();
                    Chunks[x, y, z] = chunkScript;
                }
    }

    void GenerateData()
    {
        GenerateBase();
        GenerateHazards();
        GenerateImpervious();
    }

    private void GenerateHazards()
    {
        //Generate hazards
        for (int i = 0; i < Vars.NumHazards; i++)
        {
            int xCoord = rng.Next(0, Vars.WorldWidth);
            int zCoord = rng.Next(0, Vars.WorldWidth);
            int yCoord = -1;
            for (int j = Vars.WorldHeight - 1; j >= 0; j--)
            {
                if (data[xCoord, j, zCoord] != (byte)BlockTypes.Air)
                {
                    yCoord = j;
                    break;
                }
            }

            //Generate a sphere around this point overriding non-air blocks
            for (int HX = xCoord - Vars.HazardRadius; HX < xCoord + Vars.HazardRadius; HX++)
            {
                for (int HZ = zCoord - Vars.HazardRadius; HZ < zCoord + Vars.HazardRadius; HZ++)
                {
                    for (int HY = yCoord - Vars.HazardRadius; HY < yCoord + Vars.HazardRadius; HY++)
                    {
                        float xSquare = (xCoord - HX) * (xCoord - HX);
                        float ySquare = (yCoord - HY) * (yCoord - HY);
                        float zSquare = (zCoord - HZ) * (zCoord - HZ);
                        float Dist = Mathf.Sqrt(xSquare + ySquare + zSquare);
                        if (Dist <= Vars.HazardRadius)
                        {
                            int CX, CY, CZ;
                            CX = Mathf.Clamp(HX, 1, Vars.WorldWidth - 2);
                            CZ = Mathf.Clamp(HZ, 1, Vars.WorldWidth - 2);
                            CY = Mathf.Clamp(HY, 1, Vars.WorldHeight - 2);
                            if (data[CX, CY, CZ] != (byte)BlockTypes.Air)
                                data[CX, CY, CZ] = (byte)BlockTypes.Hazard;
                        }
                    }
                }
            }
        }
    }

    private void GenerateImpervious()
    {

        //Generate hazards
        for (int i = 0; i < Vars.NumImpervious; i++)
        {
            int xCoord = rng.Next(0, Vars.WorldWidth);
            int zCoord = rng.Next(0, Vars.WorldWidth);
            int yCoord = -1;
            for (int j = Vars.WorldHeight - 1; j >= 0; j--)
            {
                if (data[xCoord, j, zCoord] != (byte)BlockTypes.Air)
                {
                    yCoord = j;
                    break;
                }
            }

            //Generate a sphere around this point overriding non-air blocks
            for (int HX = xCoord - Vars.ImperviousRadius; HX < xCoord + Vars.ImperviousRadius; HX++)
            {
                for (int HZ = zCoord - Vars.ImperviousRadius; HZ < zCoord + Vars.ImperviousRadius; HZ++)
                {
                    for (int HY = yCoord - Vars.ImperviousRadius; HY < yCoord + Vars.ImperviousRadius; HY++)
                    {
                        float xSquare = (xCoord - HX) * (xCoord - HX);
                        float ySquare = (yCoord - HY) * (yCoord - HY);
                        float zSquare = (zCoord - HZ) * (zCoord - HZ);
                        float Dist = Mathf.Sqrt(xSquare + ySquare + zSquare);
                        if (Dist <= Vars.ImperviousRadius)
                        {
                            int CX, CY, CZ;
                            CX = Mathf.Clamp(HX, 1, Vars.WorldWidth - 2);
                            CZ = Mathf.Clamp(HZ, 1, Vars.WorldWidth - 2);
                            CY = Mathf.Clamp(HY, 1, Vars.WorldHeight - 2);
                            data[CX, CY, CZ] = (byte)BlockTypes.Impervious;
                        }
                    }
                }
            }
        }
    }

    private void GenerateBase()
    {
        Noise.GenNewP(Vars.Seed);
        data = new byte[Vars.WorldWidth, Vars.WorldHeight, Vars.WorldWidth];
        for (int x = 0; x < Vars.WorldWidth; x++)
            for (int z = 0; z < Vars.WorldWidth; z++)
            {
                int stone = Vars.DiscretePerlineNoise(x, 0, z, 10, 3, 1.2f);
                stone += Vars.DiscretePerlineNoise(x, 300, z, 20, 4, 0) + 10;
                int Grass = Vars.DiscretePerlineNoise(x, 100, z, 30, 10, 0); //Added +1 to make sure minimum grass height is 1
                int Food = Vars.DiscretePerlineNoise(x, 200, z, 10, 3, 1.1f);

                for (int y = 0; y < Vars.WorldHeight; y++)
                {
                    if (y <= stone)
                    {
                        data[x, y, z] = (byte)BlockTypes.Stone;
                    }
                    else if (y <= Grass + stone)
                    {
                        data[x, y, z] = (byte)BlockTypes.Grass;
                    }
                    else if (y <= Grass + stone + Food)
                    {
                        data[x, y, z] = (byte)BlockTypes.Food;
                    }
                    if (x == 0 || x >= Vars.WorldWidth - 1 || z == 0 || z >= Vars.WorldWidth - 1 || y == 0)
                        data[x, y, z] = (byte)BlockTypes.Impervious;
                }
            }
    }

    /// <summary>
    /// Checks if an ant is occupying the desired location
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <param name="z"></param>
    /// <returns></returns>
    public int NumAntsOn(int x, int y, int z)
    {
        return ants.Where(ant => ant.X == x && ant.Y == y && ant.Z == z).Count();
    }

    public Vector3Int QueenLocation()
    {
        Queen queen = ants.First(ant => ant is Queen) as Queen;
        return new Vector3Int(queen.X, queen.Y, queen.Z);
    }

    public void UpdateChunkAt(int x, int y, int z)
    {
        //Updates the chunk containing this block
        int updateX = Mathf.FloorToInt(x / Vars.ChunkSize);
        int updateY = Mathf.FloorToInt(y / Vars.ChunkSize);
        int updateZ = Mathf.FloorToInt(z / Vars.ChunkSize);
        Chunks[updateX, updateY, updateZ].UpdateNeeded = true;


    }
}

